package it.com.atlassian.jira.plugin.ext.bamboo.utils;

import javax.annotation.Nonnull;
import java.util.Locale;

import static com.google.common.base.Preconditions.checkNotNull;

public final class JiraLocaleUtil {
    private JiraLocaleUtil() {
    }

    public static String toJiraLocale(Locale locale) {
        checkNotNull(locale);
        return locale.toLanguageTag().replace('-', '_');
    }

    public static Locale fromJiraLocale(@Nonnull final String jiraLocale) {
        checkNotNull(jiraLocale);
        return Locale.forLanguageTag(jiraLocale.replace('_', '-'));
    }
}
