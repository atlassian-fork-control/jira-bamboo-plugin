package it.com.atlassian.jira.plugin.ext.bamboo;

import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.pageobjects.config.ProductInstanceBasedEnvironmentData;
import com.atlassian.jira.software.test.licenses.DefaultJiraLicenseWrapper;
import com.atlassian.jira.software.test.licenses.JiraLicenseWrapper;
import com.atlassian.jira.test.util.lic.License;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.atlassian.jira.testkit.client.restclient.Version;
import com.atlassian.jira.testkit.client.restclient.VersionClient;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import com.google.common.collect.Lists;
import it.com.atlassian.jira.plugin.ext.bamboo.rest.VersionOperations;
import it.com.atlassian.jira.plugin.ext.bamboo.rest.VersionOperationsClient;
import org.json.JSONException;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static com.atlassian.jira.plugin.ext.bamboo.panel.BambooPanelHelper.BAMBOO_PLUGIN_KEY;
import static com.atlassian.jira.software.test.licenses.TestJiraLicenses.VALID_SOFTWARE_COMMERCIAL;
import static com.atlassian.plugin.PluginState.ENABLED;
import static it.com.atlassian.jira.plugin.ext.bamboo.utils.PluginKeys.SOFTWARE_PLUGIN_KEY;
import static it.com.atlassian.jira.plugin.ext.bamboo.utils.PluginUtil.assertPluginState;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

public class VersionOperationsTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(VersionOperationsTest.class);

    private static final String BUILD_AND_RELEASE_OPERATION_ID = "build_and_release";
    private static final String BUSINESS_PROJECT_TYPE = "business";
    private static final String SOFTWARE_PROJECT_TYPE = "software";
    private static final String VERSION_NAME = "1.0";

    private static final JIRAEnvironmentData environmentData =
            new ProductInstanceBasedEnvironmentData(JiraProductInstance.INSTANCE);
    private static final Backdoor backdoor = new Backdoor(environmentData);

    private static VersionClient versionClient;
    private static VersionOperationsClient versionOperationsClient;

    private final List<String> projectKeys = new ArrayList<>();

    @BeforeClass
    public static void setUpClass() {
        versionOperationsClient = new VersionOperationsClient(environmentData);
        versionClient = new VersionClient(environmentData);

        // some other test is disabling the software plugin indirectly, so make sure that it's enabled
        assertPluginState(backdoor, SOFTWARE_PLUGIN_KEY, ENABLED);
        assertPluginState(backdoor, BAMBOO_PLUGIN_KEY, ENABLED);
        restoreBlankInstanceWithLicense(VALID_SOFTWARE_COMMERCIAL);
    }

    private static void restoreBlankInstanceWithLicense(final JiraLicenseWrapper jiraLicenseWrapper) {
        final License license = ((DefaultJiraLicenseWrapper) jiraLicenseWrapper).get();
        backdoor.restoreBlankInstance(license.getLicenseString());
    }

    private String createProjectAndVersion(final String projectType) {
        final String projectKey = WebDriverUtils.getNewProjectKey();
        final long projectId = createProject(projectType, projectKey);
        projectKeys.add(projectKey);

        Version version = new Version().projectId(projectId).project(projectKey).name(VERSION_NAME);
        versionClient.create(version);

        return projectKey;
    }

    private long createProject(final String projectType, final String projectKey) {
        try {
            return backdoor.project().addProject("Test " + projectKey, projectKey,
                    FunctTestConstants.ADMIN_USERNAME, projectType);
        } catch (final RuntimeException e) {
            LOGGER.error("Cannot create a {} project with key = '{}'", projectType, projectKey);
            throw e;
        }
    }

    @After
    public void tearDown() {
        projectKeys.stream().forEach(projectKey -> backdoor.project().deleteProject(projectKey));
    }

    @Test
    public void testBuildAndReleaseOperationExistsOnSoftwareProject() throws JSONException {
        // having
        String projectKey = createProjectAndVersion(SOFTWARE_PROJECT_TYPE);

        // when
        Response<VersionOperations[]> response = versionOperationsClient.getOperations(projectKey);

        // then
        assertOneVersionReturned(response);
        assertThat(getOperationIds(response), hasItem(BUILD_AND_RELEASE_OPERATION_ID));
    }

    @Test
    public void testNoBuildAndReleaseOperationOnNonSoftwareProject() {
        // having
        String projectKey = createProjectAndVersion(BUSINESS_PROJECT_TYPE);

        // when
        Response<VersionOperations[]> response = versionOperationsClient.getOperations(projectKey);

        // then
        assertOneVersionReturned(response);
        assertThat(getOperationIds(response), not(hasItem(BUILD_AND_RELEASE_OPERATION_ID)));
    }

    private List<String> getOperationIds(final Response<VersionOperations[]> response) {
        return Lists.transform(getVersionOperations(response).getOperations(), VersionOperations.Operation::getId);
    }

    private VersionOperations getVersionOperations(Response<VersionOperations[]> response) {
        return response.body[0];
    }

    private void assertOneVersionReturned(final Response<VersionOperations[]> response) {
        assertThat(response.statusCode, equalTo(200));
        assertThat(response.body, notNullValue());
        assertThat(response.body.length, equalTo(1));
    }
}
