package it.com.atlassian.jira.plugin.ext.bamboo.pageobjects;

import com.atlassian.jira.projects.pageobjects.webdriver.page.legacy.AbstractProjectTab;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

import javax.inject.Inject;

public class BambooBuildsProjectTab extends AbstractProjectTab {

    private static final String LINK_ID = "com.atlassian.jira.plugin.ext.bamboo:bamboo-project-tabpanel-panel";
    private static final String DEFAULT_PROJECT_KEY = "TEST";
    @ElementBy(id = "bambooTabPanel")
    private PageElement buildsDiv;

    @Inject
    private PageBinder pageBinder;

    public BambooBuildsProjectTab(String linkId, String projectKey) {
        super(linkId, projectKey);
    }

    public BambooBuildsProjectTab(String projectKey) {
        super(LINK_ID, projectKey);
    }

    public BambooBuildsProjectTab() {
        super(LINK_ID, DEFAULT_PROJECT_KEY);
    }

}
