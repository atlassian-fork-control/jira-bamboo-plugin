package it.com.atlassian.jira.plugin.ext.bamboo.utils;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.testkit.client.BackdoorControl;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.restclient.Response;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import javax.annotation.Nonnull;

import static java.net.HttpURLConnection.HTTP_OK;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;

/**
 * Client for the privacy policy acknowledgement.
 *
 * @since v6.4
 */
public class PrivacyPolicyAcknowledgeControl extends BackdoorControl<PrivacyPolicyAcknowledgeControl> {
    /**
     * Acknowledges the privacy policy for the given Jira instance.
     *
     * @param jira the Jira instance
     */
    public static void acknowledgePrivacyPolicy(@Nonnull final JiraTestedProduct jira) {
        new PrivacyPolicyAcknowledgeControl(jira.environmentData()).acknowledgePrivacyPolicy();
    }

    private final String rootPath;

    /**
     * Constructs a new PrivacyPolicyAcknowledgeControl for a Jira instance.
     *
     * @param environmentData The Jira environment data
     */
    public PrivacyPolicyAcknowledgeControl(final JIRAEnvironmentData environmentData) {
        super(environmentData);
        this.rootPath = environmentData.getBaseUrl().toExternalForm();
    }

    /**
     * Acknowledges the privacy policy.
     */
    public void acknowledgePrivacyPolicy() {
        final Response response = toResponse(new Method() {
            @Override
            public ClientResponse call() {
                return createAcknowledgeResource().type(APPLICATION_JSON_TYPE).put(ClientResponse.class);
            }
        });

        if (response.statusCode != HTTP_OK) {
            throw new IllegalStateException("Unexpected response status code :" + response.statusCode);
        }
    }

    /**
     * Returns a WebResponse for the privacy policy acknowledgement resource
     *
     * @return a WebResource
     */
    private WebResource createAcknowledgeResource() {
        return resourceRoot(rootPath).path("rest").path("analytics").path("1.0").path("config").path("acknowledge");
    }
}
