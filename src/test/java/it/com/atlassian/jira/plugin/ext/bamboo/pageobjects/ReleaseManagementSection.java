package it.com.atlassian.jira.plugin.ext.bamboo.pageobjects;

import com.atlassian.jira.plugin.ext.bamboo.model.BuildState;
import com.atlassian.jira.projects.pageobjects.webdriver.page.legacy.browseversion.BrowseVersionTab;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.concurrent.TimeUnit;

import static com.atlassian.pageobjects.elements.query.Conditions.and;
import static com.atlassian.pageobjects.elements.query.Conditions.not;
import static com.atlassian.pageobjects.elements.query.Conditions.or;
import static com.atlassian.pageobjects.elements.query.Poller.by;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;

public class ReleaseManagementSection implements BrowseVersionTab {
    private static final String BAMBOO_ICON_SUCCESSFUL_PARTIAL = "bamboo-icon-SuccessfulPartial";
    private static final String BAMBOO_ICON_SUCCESSFUL = "bamboo-icon-Successful";
    private static final String BAMBOO_ICON_FAILED = "bamboo-icon-Failed";
    private static final String BAMBOO_ICON_UNKNOWN = "bamboo-icon-Unknown";
    private static final String BAMBOO_ICON_IN_PROGRESS = "bamboo-icon-InProgress";
    private static final String BAMBOO_ICON_QUEUED = "bamboo-icon-Queued";
    private static final String BAMBOO_ICON_LOADING = "bamboo-icon-loading";
    private static final String RUN_RELEASE_BUTTON_CSS_SELECTOR = ".aui-page-panel-content #runRelease";

    @Inject
    private Timeouts timeouts;

    @Inject
    private PageBinder pageBinder;

    @Inject
    private PageElementFinder elementFinder;

    @ElementBy(cssSelector = RUN_RELEASE_BUTTON_CSS_SELECTOR)
    private PageElement runReleaseButton;

    @ElementBy(id = "build-details")
    private PageElement buildDetails;

    @ElementBy(id = "release-form-date-picker")
    private PageElement releaseFormDatePicker;

    private PageElement buildStatusIcon;

    @Init
    private void init() {
        buildStatusIcon = buildDetails.find(By.className("bamboo-icon"));
    }

    public String linkId() {
        return "com.atlassian.jira.plugins.jira-development-integration-plugin:release-report-tabpanel-panel";
    }

    public TimedCondition isOpen() {
        // this won't work in case app link to bamboo is missing. if you need to test for that case, add page element
        // for the button and corresponding or here
        return Conditions.or(runReleaseButton.timed().isPresent(), buildDetails.timed().isPresent());
    }

    public TimedCondition isVersionReleased() {
        return and(buildDetails.timed().isPresent());
    }

    /**
     * Open the release dialog and wait till the UI reflects this change.
     *
     * @param bambooConnected true if bamboo is connected to Jira and false otherwise
     * @return the release dialog page object
     */
    public ReleaseDialog openReleaseDialog(final boolean bambooConnected) {
        waitUntilTrue(runReleaseButton.withTimeout(TimeoutType.PAGE_LOAD).timed().isVisible());
        runReleaseButton.click();
        final ReleaseDialog releaseDialog = pageBinder.bind(ReleaseDialog.class);
        waitUntil(null, releaseDialog.isOpen(bambooConnected), is(true), by(60, TimeUnit.SECONDS));
        return releaseDialog;
    }

    public TimedCondition hasBuildStatus() {
        return and(isVersionReleased(), buildStatusIcon.timed().isPresent());
    }

    public TimedCondition isBuildRunning() {
        return and(hasBuildStatus(),
                or(buildStatusIcon.timed().hasClass(BAMBOO_ICON_IN_PROGRESS),
                        buildStatusIcon.timed().hasClass(BAMBOO_ICON_QUEUED),
                        buildStatusIcon.timed().hasClass(BAMBOO_ICON_LOADING)
                ));
    }

    public TimedCondition isBuildFinished() {
        return and(hasBuildStatus(), not(isBuildRunning()));
    }

    public ReleaseManagementSection waitForBuildToFinish(TimeoutType howLong) {
        waitUntil("Build has not finished by " + howLong, isBuildFinished(), is(true), by(timeouts.timeoutFor(howLong)));
        return this;
    }

    public BuildState getBuildStatus() {
        assertTrue("Build must be finished", isBuildFinished().now());
        if (!buildStatusIcon.hasClass(BAMBOO_ICON_UNKNOWN)) {
            if (buildStatusIcon.hasClass(BAMBOO_ICON_SUCCESSFUL_PARTIAL) || buildStatusIcon.hasClass(BAMBOO_ICON_SUCCESSFUL)) {
                return BuildState.SUCCESS;
            } else if (buildStatusIcon.hasClass(BAMBOO_ICON_FAILED)) {
                return BuildState.FAILED;
            }
        } else {
            return BuildState.UNKNOWN;
        }
        throw new AssertionError("Error trying to get Build Status. CSS classes for buildStatusIcon: " + buildStatusIcon.getAttribute("class") + ")");
    }

    /**
     * Execute a new release.
     * <p/>
     * This method assumes that Bamboo is connected to Jira.
     *
     * @param planKey the plan key
     * @param stages  list of stages
     * @return the build state page object
     */
    public BuildState executeNewRelease(String planKey, String... stages) {
        final ReleaseDialog releaseDialog = openReleaseDialog(true);
        releaseDialog.selectNewBuild(planKey).unselectAllStages().selectStages(stages).submit(true);
        return waitForBuildToFinish(TimeoutType.PAGE_LOAD).getBuildStatus();
    }

    public ReleaseDialog releaseWithNoBuild() {
        final ReleaseDialog releaseDialog = openReleaseDialog(true);
        releaseDialog.selectNoBuild().submit(false);
        return releaseDialog;
    }

    /**
     * Execute an existing build for the release.
     * <p/>
     * This method assumes that Bamboo is connected to Jira.
     *
     * @param planKey     the plan key
     * @param buildNumber the build number
     * @param stages      list of stages
     * @return the build state page object
     */
    public BuildState executeExistingBuildForRelease(String planKey, int buildNumber, String... stages) {
        openReleaseDialog(true).selectExistingBuild(planKey).selectBuildNumber(buildNumber).unselectAllStages()
                .selectStages(stages).submit(true);
        return waitForBuildToFinish(TimeoutType.PAGE_LOAD).getBuildStatus();
    }

    public TimedCondition releaseButtonIsVisible() {
        return runReleaseButton.withTimeout(TimeoutType.PAGE_LOAD).timed().isVisible();
    }

}
