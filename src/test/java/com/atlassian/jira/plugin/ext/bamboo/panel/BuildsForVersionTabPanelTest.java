package com.atlassian.jira.plugin.ext.bamboo.panel;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.plugin.ext.bamboo.service.BambooReleaseService;
import com.atlassian.jira.plugin.versionpanel.BrowseVersionContext;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.software.api.conditions.ProjectDevToolsIntegrationFeatureCondition;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.atlassian.jira.issue.IssueFieldConstants.FIX_FOR_VERSIONS;
import static com.atlassian.jira.plugin.ext.bamboo.panel.BambooPanelHelper.SUB_TABS;
import static com.atlassian.jira.plugin.ext.bamboo.panel.BuildsForVersionTabPanel.MODULE_KEY;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyMapOf;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BuildsForVersionTabPanelTest {
    private static final long PROJECT_ID = 123456;
    private static final long VERSION_ID = 156789;
    private static final String PROJECT_KEY = "TST";

    @Mock
    private BambooPanelHelper bambooPanelHelper;
    @Mock
    private BambooReleaseService bambooReleaseService;
    @Mock
    private FeatureManager featureManager;
    @Mock
    private FieldVisibilityManager fieldVisibilityManager;
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private PermissionManager permissionManager;
    @InjectMocks
    private BuildsForVersionTabPanel panel;

    @Mock
    private ApplicationUser applicationUser;
    @Mock
    private BrowseVersionContext context;
    @Mock
    private Project project;
    @Mock
    private Version version;
    @Mock
    private ProjectDevToolsIntegrationFeatureCondition projectDevToolsIntegrationFeatureCondition;

    @Before
    public void setUp() {
        when(authenticationContext.getLoggedInUser()).thenReturn(applicationUser);
        when(context.getProject()).thenReturn(project);
        when(context.getVersion()).thenReturn(version);
        when(project.getId()).thenReturn(PROJECT_ID);
    }

    @Test
    public void panelShouldNotBeShownIfFieldIsNotVisible() {
        assertShowPanel(true, false, true, false);
    }

    @Test
    public void panelShouldNotBeShownIfUserDoesNotHaveViewVersionControlPermission() {
        assertShowPanel(true, true, false, false);
    }

    @Test
    public void panelShouldNotBeShownIfSoftwarePermissionsAreNotSatisfied() {
        assertShowPanel(true, true, true, false, false);
    }

    @Test
    public void panelShouldBeShownIfAllPreconditionsAreMet() {
        assertShowPanel(true, true, true, true);
    }

    // This is testing an anti-pattern, see BDEV-5994
    @Test
    public void decidingWhetherToShowPanelShouldResetReleaseStateIfVersionWasUnreleased() {
        // Invoke
        panel.addVelocityParams(context, Maps.<String, Object>newHashMap());

        // Check
        verify(bambooReleaseService).resetReleaseStateIfVersionWasUnreleased(version);
    }

    @Test
    public void panelShouldNotBeShownIfBambooIsNotAppLinked() {
        assertShowPanel(false, true, true, false);
    }


    @Test
    public void panelShouldPutExtraDescriptionKeyIfVersionIsReleasedAndHasReleaseDate() {
        assertVelocityParams(new Date(), true, ImmutableMap.of("extraDescriptionKey", (Object) "released."));
    }

    @Test
    public void panelShouldNotPutExtraDescriptionKeyIfVersionHasNoReleaseDate() {
        assertVelocityParams(null, true, Collections.<String, Object>emptyMap());
    }

    @Test
    public void panelShouldNotPutExtraDescriptionKeyIfVersionIsNotReleased() {
        assertVelocityParams(new Date(), false, Collections.<String, Object>emptyMap());
    }

    @Test
    public void panelShouldPassCorrectParametersToBambooPanelHelper() {
        // Set up
        final Map<String, Object> velocityParams = ImmutableMap.of("foo", new Object());
        final String baseLinkUrl = "someUrl";
        final String queryString = "someQuery";

        // Invoke
        panel.prepareVelocityContext(velocityParams, baseLinkUrl, queryString, project);

        // Check
        verify(bambooPanelHelper).prepareVelocityContext(
                velocityParams, MODULE_KEY, baseLinkUrl, queryString, SUB_TABS, project);
    }

    private void setUpBambooAppLinked(final boolean linked) {
        when(bambooPanelHelper.isBambooAppLinked()).thenReturn(linked);
    }

    private void setUpFieldVisible(final boolean visible) {
        // We reverse the sense here to make the calling code more readable
        when(fieldVisibilityManager.isFieldHiddenInAllSchemes(PROJECT_ID, FIX_FOR_VERSIONS))
                .thenReturn(!visible);
    }

    private void setUpViewVersionControlPermission(final boolean permitted) {
        when(permissionManager.hasPermission(ProjectPermissions.VIEW_DEV_TOOLS, project, applicationUser))
                .thenReturn(permitted);
    }

    private void setUpSoftwareConditions(final boolean softwarePermissions) {
        when(projectDevToolsIntegrationFeatureCondition.shouldDisplay(anyMapOf(String.class, Object.class)))
                .thenReturn(softwarePermissions);
    }

    private void assertShowPanel(final boolean bambooAppLinked, final boolean fieldVisible,
                                 final boolean viewVersionControlPermission,
                                 final boolean expectedShowPanel) {
        assertShowPanel(bambooAppLinked, fieldVisible, viewVersionControlPermission, true, expectedShowPanel);
    }

    private void assertShowPanel(final boolean bambooAppLinked, final boolean fieldVisible,
                                 final boolean viewVersionControlPermission,
                                 final boolean softwarePermissions,
                                 final boolean expectedShowPanel) {
        // Set up
        setUpBambooAppLinked(bambooAppLinked);
        setUpFieldVisible(fieldVisible);
        setUpViewVersionControlPermission(viewVersionControlPermission);
        setUpSoftwareConditions(softwarePermissions);

        // Invoke
        final boolean showPanel = panel.showPanel(context);

        // Check
        assertThat(showPanel, is(expectedShowPanel));
    }

    private void assertVelocityParams(
            final Date releaseDate, final boolean released, final Map<String, Object> expectedVelocityParams) {
        // Set up
        final Map<String, Object> velocityParams = new HashMap<>();
        when(version.getReleaseDate()).thenReturn(releaseDate);
        when(version.isReleased()).thenReturn(released);

        // Invoke
        panel.addVelocityParams(context, velocityParams);

        // Check
        assertThat(velocityParams, is(expectedVelocityParams));
    }
}
