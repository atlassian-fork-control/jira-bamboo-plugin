package com.atlassian.jira.plugin.ext.bamboo.view;

import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.plugin.ext.bamboo.applinks.BambooApplicationLinkManager;
import com.atlassian.jira.plugin.ext.bamboo.panel.BambooPanelHelper;
import com.atlassian.jira.plugin.ext.bamboo.service.BambooReleaseService;
import com.atlassian.jira.plugin.versionpanel.BrowseVersionContext;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.software.api.conditions.ProjectDevToolsIntegrationFeatureCondition;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.annotation.Nonnull;
import java.util.Map;

import static com.atlassian.jira.plugin.ext.bamboo.view.BambooVersionTabPanel.SHOW_FUSION_FEEDBACK_BANNERS_KEY;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * Unit test of the abstract BambooVersionTabPanel class.
 */
@RunWith(MockitoJUnitRunner.class)
public class BambooVersionTabPanelTest {
    private static final String MODULE_KEY = "someModuleKey";
    private static final String PROJECT_KEY = "FUZE";
    private static final long VERSION_ID = 54321;

    @Mock
    private BambooApplicationLinkManager bambooApplicationLinkManager;
    @Mock
    private BambooPanelHelper bambooPanelHelper;
    @Mock
    private BambooReleaseService bambooReleaseService;
    @Mock
    private BrowseVersionContext context;
    @Mock
    private FeatureManager featureManager;
    @Mock
    private FieldVisibilityManager fieldVisibilityManager;
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private Project project;
    @Mock
    private ProjectDevToolsIntegrationFeatureCondition projectDevToolsIntegrationFeatureCondition;
    @Mock
    private SearchProvider searchProvider;
    @Mock
    private Version version;

    @InjectMocks
    private PanelSpy panel;

    @Before
    public void setUp() {
        when(context.getProject()).thenReturn(project);
        when(context.getVersion()).thenReturn(version);
        when(project.getKey()).thenReturn(PROJECT_KEY);
        when(version.getId()).thenReturn(VERSION_ID);
    }

    @Test
    public void shouldCreateCorrectDefaultVelocityParams() {
        // Invoke
        final Map<String, Object> velocityParams = panel.createVelocityParams(context);

        // Check
        final Map<String, Object> expectedVelocityParams = ImmutableMap.of(
                "project", project,
                "versionContext", context,
                SHOW_FUSION_FEEDBACK_BANNERS_KEY, false
        );
        assertThat(velocityParams, is(expectedVelocityParams));
        assertThat(panel.baseLinkUrl,
                is("/browse/FUZE/fixforversion/54321?selectedTab=com.atlassian.jira.plugin.ext.bamboo:someModuleKey"));
        assertThat(panel.queryString, is("versionId=54321"));
    }

    /**
     * A spy panel that we can use to test the superclass logic.
     */
    private static class PanelSpy extends BambooVersionTabPanel {
        String baseLinkUrl;
        String queryString;

        protected PanelSpy(
                final BambooPanelHelper bambooPanelHelper,
                final BambooReleaseService bambooReleaseService,
                final FieldVisibilityManager fieldVisibilityManager,
                final JiraAuthenticationContext authenticationContext,
                final PermissionManager permissionManager,
                final ProjectDevToolsIntegrationFeatureCondition projectDevToolsIntegrationFeatureCondition,
                final SearchProvider searchProvider) {
            super(fieldVisibilityManager, authenticationContext, permissionManager, searchProvider,
                    projectDevToolsIntegrationFeatureCondition, bambooPanelHelper, bambooReleaseService, MODULE_KEY);
        }

        @Override
        protected void addVelocityParams(
                @Nonnull final BrowseVersionContext context, @Nonnull final Map<String, Object> velocityParams) {
            // Nothing interesting to record here
        }

        @Override
        protected void prepareVelocityContext(final Map<String, Object> velocityParams, final String baseLinkUrl,
                                              final String queryString, final Project project) {
            // Record the interesting arguments for assertion
            this.baseLinkUrl = baseLinkUrl;
            this.queryString = queryString;
        }
    }
}
