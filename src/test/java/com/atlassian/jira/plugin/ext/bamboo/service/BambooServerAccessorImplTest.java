package com.atlassian.jira.plugin.ext.bamboo.service;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.jira.plugin.ext.bamboo.applinks.BambooApplicationLinkManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.sal.api.net.ResponseException;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.URI;
import java.util.Map;
import java.util.Set;

import static com.atlassian.jira.plugin.ext.bamboo.service.BambooRestServiceImpl.TRIGGER_CALL_TIMEOUT;
import static com.atlassian.jira.plugin.ext.bamboo.service.BambooServerAccessorImpl.DEFAULT_REQUEST_PARAMS;
import static com.atlassian.jira.plugin.ext.bamboo.service.BambooServerAccessorImpl.ISSUE_KEY_PARAM;
import static com.atlassian.jira.plugin.ext.bamboo.service.BambooServerAccessorImpl.LOGIN_FORM_INDICATOR;
import static com.atlassian.jira.plugin.ext.bamboo.service.BambooServerAccessorImpl.NO_ASSOCIATED_BUILDS_I18N_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.service.BambooServerAccessorImpl.NO_BAMBOO_APPLICATION_LINK_MESSAGE;
import static com.atlassian.jira.plugin.ext.bamboo.service.BambooServerAccessorImpl.NO_CHECK;
import static com.atlassian.jira.plugin.ext.bamboo.service.BambooServerAccessorImpl.PROJECT_KEY_PARAM;
import static com.atlassian.jira.plugin.ext.bamboo.service.BambooServerAccessorImpl.X_ATLASSIAN_TOKEN;
import static com.atlassian.sal.api.net.Request.MethodType.POST;
import static java.util.Collections.emptyMap;
import static java.util.Collections.singleton;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BambooServerAccessorImplTest {

    private static final long PROJECT_ID = 12345;

    private static final String BAMBOO_ACTION = "bePanda";
    private static final String DISPLAY_URI = "http://www.example.com";
    private static final String OLD_PROJECT_KEY = "FOO";
    private static final String PROJECT_KEY = "ACME";
    private static final String KEY_1 = "key1";
    private static final String VALUE_1 = "value1";
    private static final String KEY_2 = "key2";
    private static final String VALUE_2 = "value2";

    private static final Map<String, String> EXTRA_PARAMS = ImmutableMap.of(KEY_1, VALUE_1, KEY_2, VALUE_2);

    private static void assertCommonConfiguration(final ApplicationLinkRequest applicationLinkRequest) {
        verify(applicationLinkRequest).addRequestParameters(DEFAULT_REQUEST_PARAMS);
        verify(applicationLinkRequest).addRequestParameters(KEY_1, VALUE_1);
        verify(applicationLinkRequest).addRequestParameters(KEY_2, VALUE_2);
        verify(applicationLinkRequest).setSoTimeout(TRIGGER_CALL_TIMEOUT);
        verify(applicationLinkRequest).setConnectionTimeout(TRIGGER_CALL_TIMEOUT);
        verify(applicationLinkRequest).setHeader(X_ATLASSIAN_TOKEN, NO_CHECK);
    }

    @Mock
    private ApplicationLink applicationLink;
    @Mock
    private ApplicationLinkRequestFactory applicationLinkRequestFactory;
    @Mock
    private BambooApplicationLinkManager bambooApplicationLinkManager;
    @Mock
    private BambooContentRewriter bambooContentRewriter;
    @Mock
    private I18nHelper i18nHelper;
    @Mock
    private Project project;
    @Mock
    private ProjectManager projectManager;

    @InjectMocks
    private BambooServerAccessorImpl objectUnderTest;

    @Before
    public void setUp() throws Exception {
        when(applicationLink.createAuthenticatedRequestFactory()).thenReturn(applicationLinkRequestFactory);
        when(applicationLink.getDisplayUrl()).thenReturn(URI.create(DISPLAY_URI));
        when(project.getKey()).thenReturn(PROJECT_KEY);
    }

    @Test
    public void getHtmlFromActionByProjectKeysShouldAddGivenParamsToRequest() throws Exception {
        // Set up
        final String htmlFromUrl1 = "the build_result 1";
        final String htmlFromUrl2 = "the build_result 2";
        final Set<String> allProjectKeys = ImmutableSet.of(OLD_PROJECT_KEY, PROJECT_KEY);

        when(bambooApplicationLinkManager.getApplicationLink(PROJECT_KEY)).thenReturn(applicationLink);
        when(project.getId()).thenReturn(PROJECT_ID);
        when(projectManager.getAllProjectKeys(PROJECT_ID)).thenReturn(allProjectKeys);
        final ApplicationLinkRequest appLinkRequest1 = setUpAppLinkRequest("response 1", htmlFromUrl1);
        final ApplicationLinkRequest appLinkRequest2 = setUpAppLinkRequest("response 2", htmlFromUrl2);
        when(applicationLinkRequestFactory.createRequest(POST, BAMBOO_ACTION))
                .thenReturn(appLinkRequest1, appLinkRequest2);

        // Invoke
        final String html = objectUnderTest.getHtmlFromAction(BAMBOO_ACTION, project, EXTRA_PARAMS);

        // Check
        assertThat(html, is(htmlFromUrl1 + htmlFromUrl2));
        verify(appLinkRequest1).addRequestParameters(PROJECT_KEY_PARAM, PROJECT_KEY);
        assertCommonConfiguration(appLinkRequest1);
        verify(appLinkRequest2).addRequestParameters(PROJECT_KEY_PARAM, OLD_PROJECT_KEY);
        assertCommonConfiguration(appLinkRequest2);
    }

    @Test
    public void getHtmlFromActionByProjectKeysShouldReturnTranslatedErrorWhenNoAssociatedBuilds() throws Exception {
        // Set up
        final String errorMessage = "whoops!";
        final String htmlFromUrl = "no build results here";
        final ApplicationLinkRequest appLinkRequest = setUpAppLinkRequest("some response", htmlFromUrl);

        when(bambooApplicationLinkManager.getApplicationLink(PROJECT_KEY)).thenReturn(applicationLink);
        when(i18nHelper.getText(NO_ASSOCIATED_BUILDS_I18N_KEY)).thenReturn(errorMessage);
        when(applicationLinkRequestFactory.createRequest(POST, BAMBOO_ACTION)).thenReturn(appLinkRequest);

        // Invoke
        final String html = objectUnderTest.getHtmlFromAction(BAMBOO_ACTION, project, EXTRA_PARAMS);

        // Check
        assertThat(html, is("<p>" + errorMessage + "</p>"));
        assertCommonConfiguration(appLinkRequest);
    }

    @Test(expected = ResponseException.class)
    public void getHtmlFromActionByProjectKeysShouldThrowWhenAuthenticationFails() throws Exception {
        // Set up
        final String responseHtml = "here is the " + LOGIN_FORM_INDICATOR + " to fill in";
        final ApplicationLinkRequest appLinkRequest = setUpAppLinkRequest(responseHtml, "ignored");

        when(bambooApplicationLinkManager.getApplicationLink(PROJECT_KEY)).thenReturn(applicationLink);
        when(applicationLinkRequestFactory.createRequest(POST, BAMBOO_ACTION)).thenReturn(appLinkRequest);

        // Invoke
        objectUnderTest.getHtmlFromAction(BAMBOO_ACTION, project, emptyMap());
    }

    @Test
    public void getHtmlFromActionByIssueKeysShouldAddGivenParamsToRequest() throws Exception {
        // Set up
        final String htmlFromUrl = "some HTML";
        final ApplicationLinkRequest appLinkRequest = setUpAppLinkRequest("some response", htmlFromUrl);
        final String issueKey1 = "ACME-51";
        final String issueKey2 = "ACME-50";
        final Iterable<String> issueKeys = ImmutableSet.of(issueKey1, issueKey2);

        when(bambooApplicationLinkManager.getApplicationLink(PROJECT_KEY)).thenReturn(applicationLink);
        when(applicationLinkRequestFactory.createRequest(POST, BAMBOO_ACTION)).thenReturn(appLinkRequest);

        // Invoke
        final String html = objectUnderTest.getHtmlFromAction(BAMBOO_ACTION, project, issueKeys, EXTRA_PARAMS);

        // Check
        assertThat(html, is(htmlFromUrl));
        verify(appLinkRequest).addRequestParameters(ISSUE_KEY_PARAM, issueKey1);
        verify(appLinkRequest).addRequestParameters(ISSUE_KEY_PARAM, issueKey2);
        assertCommonConfiguration(appLinkRequest);
    }

    @Test
    public void htmlShouldBeErrorMessageIfAppLinkIsNull() throws Exception {
        // Set up
        when(bambooApplicationLinkManager.getApplicationLink(PROJECT_KEY)).thenReturn(null);

        // Invoke
        final String html = objectUnderTest.getHtmlFromAction(BAMBOO_ACTION, project, singleton("ACME-52"), emptyMap());

        // Check
        assertThat(html, is(NO_BAMBOO_APPLICATION_LINK_MESSAGE));
    }

    private ApplicationLinkRequest setUpAppLinkRequest(final String responseHtml, final String htmlFromUrl)
            throws Exception {
        final ApplicationLinkRequest applicationLinkRequest = mock(ApplicationLinkRequest.class);
        when(applicationLinkRequest.execute()).thenReturn(responseHtml);
        when(bambooContentRewriter.rewriteHtml(responseHtml, DISPLAY_URI)).thenReturn(htmlFromUrl);
        return applicationLinkRequest;
    }
}
