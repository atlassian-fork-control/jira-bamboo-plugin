package com.atlassian.jira.plugin.ext.bamboo.model;

import org.apache.commons.lang.builder.CompareToBuilder;

public class BambooDeploymentProject implements Comparable<BambooDeploymentProject> {
    private final long deploymentProjectId;
    private final String deploymentProjectKey;
    private final String deploymentProjectName;

    public BambooDeploymentProject(final long deploymentProjectId, final String deploymentProjectKey, final String deploymentProjectName) {
        this.deploymentProjectId = deploymentProjectId;
        this.deploymentProjectKey = deploymentProjectKey;
        this.deploymentProjectName = deploymentProjectName;
    }

    public long getDeploymentProjectId() {
        return deploymentProjectId;
    }

    public String getDeploymentProjectKey() {
        return deploymentProjectKey;
    }

    public String getDeploymentProjectName() {
        return deploymentProjectName;
    }

    public int compareTo(final BambooDeploymentProject o) {
        return new CompareToBuilder()
                .append(deploymentProjectId, o.deploymentProjectId)
                .toComparison();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final BambooDeploymentProject that = (BambooDeploymentProject) o;

        if (deploymentProjectId != that.deploymentProjectId) {
            return false;
        }
        if (!deploymentProjectKey.equals(that.deploymentProjectKey)) {
            return false;
        }
        if (!deploymentProjectName.equals(that.deploymentProjectName)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (deploymentProjectId ^ (deploymentProjectId >>> 32));
        result = 31 * result + deploymentProjectKey.hashCode();
        result = 31 * result + deploymentProjectName.hashCode();
        return result;
    }
}
