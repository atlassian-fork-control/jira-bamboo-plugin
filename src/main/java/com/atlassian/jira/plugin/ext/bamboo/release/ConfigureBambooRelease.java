package com.atlassian.jira.plugin.ext.bamboo.release;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.CredentialsRequiredException;
import io.atlassian.fugue.Either;
import com.atlassian.jira.bc.project.version.VersionBuilder;
import com.atlassian.jira.bc.project.version.VersionService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.datetime.DateTimeFormatUtils;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.jira.plugin.ext.bamboo.applinks.BambooApplicationLinkManager;
import com.atlassian.jira.plugin.ext.bamboo.model.BambooPlan;
import com.atlassian.jira.plugin.ext.bamboo.model.BambooProject;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanKey;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanKeys;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanResultKey;
import com.atlassian.jira.plugin.ext.bamboo.model.RestResult;
import com.atlassian.jira.plugin.ext.bamboo.service.BambooReleaseService;
import com.atlassian.jira.plugin.ext.bamboo.service.BambooRestService;
import com.atlassian.jira.plugin.ext.bamboo.service.PlanExecutionResult;
import com.atlassian.jira.plugin.ext.bamboo.service.ProjectVersionService;
import com.atlassian.jira.plugin.ext.bamboo.web.ActionRequestData;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.DateFieldFormat;
import com.atlassian.jira.web.action.ActionViewData;
import com.atlassian.jira.web.action.ProjectActionSupport;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.json.JSONException;
import org.json.JSONObject;
import webwork.action.ActionContext;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.net.URI;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import static com.atlassian.jira.bc.project.version.VersionService.VersionBuilderValidationResult;
import static com.atlassian.jira.permission.ProjectPermissions.VIEW_DEV_TOOLS;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.ADMIN_ERROR_VERSION_NO_PERMISSION_I18N_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.BAMBOO_ERROR_CONNECTIVITY_CONTINUE_RELEASE_I18N_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.BUILD_TYPE_EXISTING_BUILD;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.BUILD_TYPE_NEW_BUILD;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.BUILD_TYPE_NO_BUILD;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.ERROR_CONFIGURATION_I18N_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.ERROR_INVALID_PLAN_I18N_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.ERROR_INVALID_VERSION_I18N_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.ERROR_USER_NOT_AUTHORISED_I18N_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.ERROR_VERSION_WITHOUT_PROJECT_I18N_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.ISSUE_ACTION_MOVE;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.JIRA_ERROR_NOT_RECOVERABLE_I18N_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.PS_CONFIG_BUILD_TYPE;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.PS_CONFIG_OPEN_ISSUES;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.PS_CONFIG_OPEN_ISSUES_VERSION;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.PS_CONFIG_PLAN;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.PS_CONFIG_RELEASE_DATE;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.PS_CONFIG_STAGE;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.PS_CONFIG_USER_NAME;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.VARIABLE_PARAM_PREFIX;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.stream.Collectors.toMap;
import static org.apache.commons.lang.StringUtils.isNotEmpty;

/**
 * A Webwork action that allows the user to configure a Bamboo release.
 */
@SuppressWarnings("unused")
public class ConfigureBambooRelease extends ProjectActionSupport {
    @VisibleForTesting
    static final String INCORRECT_DATE_FORMAT = "admin.errors.incorrect.date.format";

    @VisibleForTesting
    static final String RELEASE_DATE_FIELD = "releaseDate";

    private final BambooApplicationLinkManager bambooApplicationLinkManager;
    private final BambooReleaseService bambooReleaseService;
    private final BambooRestService bambooRestService;
    private final DateFieldFormat dateFieldFormat;
    private final PermissionManager permissionManager;
    private final ProjectVersionService projectVersionService;
    private final VersionManager versionManager;
    private final VersionService versionService;

    private boolean bambooLinked;
    private int openIssueCount;
    private long versionId;
    private Collection<Version> versions;
    private Date releaseDate;
    private Long moveUnfixedIssuesTo;
    private Map<BambooProject, List<BambooPlan>> plansByProject;
    private String buildResult;
    private String buildType;

    // Variables were initialised to avoid weird rendering in the velocity template such as <input value="$currentReleaseDate" />
    // rather than <input value="" />
    private String currentReleaseDate = "";
    private String formattedReleaseDate = "";
    private String dateFormat = "";
    private String selectedPlanKey;
    private String selectedStages;
    private String unresolved;
    private String userReleaseDate;
    private String variablesJson;
    private URI credentialsUrl;
    private Version version;
    private String redirectUrlOnSuccess;
    private String applicationLinkName;

    public ConfigureBambooRelease(
            @ComponentImport final DateFieldFormat dateFieldFormat,
            @ComponentImport final PermissionManager permissionManager,
            @ComponentImport final ProjectManager projectManager,
            @ComponentImport final VersionManager versionManager,
            @ComponentImport final VersionService versionService,
            final BambooApplicationLinkManager bambooApplicationLinkManager,
            final BambooReleaseService bambooReleaseService,
            final BambooRestService bambooRestService,
            final ProjectVersionService projectVersionService) {
        super(projectManager, permissionManager);
        this.bambooApplicationLinkManager = checkNotNull(bambooApplicationLinkManager);
        this.bambooReleaseService = checkNotNull(bambooReleaseService);
        this.bambooRestService = checkNotNull(bambooRestService);
        this.dateFieldFormat = checkNotNull(dateFieldFormat);
        this.permissionManager = checkNotNull(permissionManager);
        this.projectVersionService = checkNotNull(projectVersionService);
        this.versionManager = checkNotNull(versionManager);
        this.versionService = checkNotNull(versionService);
    }

    /**
     * Called reflectively by webwork.action.ActionSupport#invokeCommand() as the "input" command.
     * Must take no parameters and return a String.
     */
    public String doInput() {
        applicationLinkName = "";

        version = versionManager.getVersion(versionId);
        if (version == null) {
            addErrorMessage(getI18nText(ERROR_INVALID_VERSION_I18N_KEY, String.valueOf(versionId)));
            return INPUT;
        }

        setupDateFormat();

        final Project projectObject = version.getProject();
        openIssueCount = getUnresolvedIssues(version.getProjectId(), versionId);
        versions = versionManager.getVersionsUnreleased(version.getProjectId(), true);
        versions.remove(version);

        final ApplicationLink applicationLink = bambooApplicationLinkManager.getApplicationLink(projectObject.getKey());
        bambooLinked = (applicationLink != null) && permissionManager.hasPermission(VIEW_DEV_TOOLS, projectObject, getLoggedInUser());

        if (bambooLinked) {
            applicationLinkName = applicationLink.getName();

            final ApplicationLinkRequestFactory authenticatedRequestFactory = applicationLink.createAuthenticatedRequestFactory();
            if (authenticatedRequestFactory == null) {
                addErrorMessage(getI18nText(ERROR_CONFIGURATION_I18N_KEY));
            } else {
                // setup data for config
                try {
                    RestResult<Map<BambooProject, List<BambooPlan>>> result = bambooRestService.getPlanList(applicationLink, false);
                    plansByProject = result.getResult();
                    if (plansByProject == null || !result.getErrors().isEmpty()) {
                        // Note: We are deliberately ignoring error details and showing a generic error to the user
                        addErrorMessage(getI18nText(BAMBOO_ERROR_CONNECTIVITY_CONTINUE_RELEASE_I18N_KEY, applicationLink.getName()));
                    }
                } catch (CredentialsRequiredException e) {
                    log.debug(e.getMessage(), e);

                    // Seems this was not able to be injected via a setter.
                    final ApplicationProperties applicationProperties =
                            ComponentAccessor.getOSGiComponentInstanceOfType(ApplicationProperties.class);

                    String baseLinkUrl = getBaseUrl(projectObject, version);
                    String jiraBaseUrl = applicationProperties.getBaseUrl(UrlMode.CANONICAL);
                    credentialsUrl = e.getAuthorisationURI(URI.create(jiraBaseUrl + baseLinkUrl));
                    return INPUT;
                }
            }
        }

        Map<String, String> previousConfiguration = bambooReleaseService.getConfigData(projectObject.getKey(), versionId);
        if (previousConfiguration != null) {
            useSettingsAsDefaults(previousConfiguration);
        } else {
            useSettingsAsDefaults(bambooReleaseService.getDefaultSettings(projectObject.getKey()));
        }

        // Set default build type
        if (StringUtils.isBlank(buildType)) {
            buildType = BUILD_TYPE_NEW_BUILD;
        }

        return INPUT;
    }

    private void setupDateFormat() {
        dateFormat = DateTimeFormatUtils.getDateFormat();
        if (version.getReleaseDate() != null) {
            DateTimeFormatter dateTimeFormatter = getDateTimeFormatter().withStyle(DateTimeStyle.DATE_PICKER)
                    .withZone(TimeZone.getDefault());
            formattedReleaseDate = dateTimeFormatter.format(version.getReleaseDate());
        } else {
            formattedReleaseDate = "";
        }
    }

    private void useSettingsAsDefaults(@Nonnull final Map<String, String> settings) {
        buildType = settings.get(PS_CONFIG_BUILD_TYPE);
        selectedPlanKey = settings.get(PS_CONFIG_PLAN);
        unresolved = settings.get(PS_CONFIG_OPEN_ISSUES);
        moveUnfixedIssuesTo = NumberUtils.createLong(settings.get(PS_CONFIG_OPEN_ISSUES_VERSION));
        selectedStages = settings.get(PS_CONFIG_STAGE);

        final Map<String, String> variables = bambooReleaseService.getBambooVariablesFromMap(settings, "");
        if (!variables.isEmpty()) {
            JSONObject variablesJsonObject = new JSONObject();
            try {
                variablesJsonObject.put("variables", variables);
                variablesJson = variablesJsonObject.toString();
            } catch (JSONException e) {
                // don't do anything we don't really care.
            }
        }
    }

    protected String doExecute() {
        version = versionManager.getVersion(versionId);
        final Project projectObject = version.getProject();
        final Map<String, String> settings = new HashMap<>();

        final ApplicationUser user = getLoggedInUser();
        settings.put(PS_CONFIG_USER_NAME, user.getName());
        settings.put(PS_CONFIG_OPEN_ISSUES, unresolved);

        // FUSE-2219: use DateFieldFormat#formatDatePicker and NOT DateFieldFormat#format.
        settings.put(PS_CONFIG_RELEASE_DATE, dateFieldFormat.formatDatePicker(releaseDate));

        if (ISSUE_ACTION_MOVE.equals(unresolved)) {
            settings.put(PS_CONFIG_OPEN_ISSUES_VERSION, moveUnfixedIssuesTo.toString());
        }

        //Set default build type
        if (StringUtils.isBlank(buildType)) {
            buildType = BUILD_TYPE_NO_BUILD;
        }

        settings.put(PS_CONFIG_BUILD_TYPE, buildType);

        final boolean existingBuild = BUILD_TYPE_EXISTING_BUILD.equals(buildType);
        final boolean newBuild = BUILD_TYPE_NEW_BUILD.equals(buildType);
        if (newBuild || existingBuild) {
            PlanKey planKey = PlanKeys.getPlanKey(selectedPlanKey);


            settings.put(PS_CONFIG_PLAN, planKey.getKey());
            if (StringUtils.isNotBlank(selectedStages)) {
                settings.put(PS_CONFIG_STAGE, selectedStages);
            }

            final Map<String, String> variables = filterVariableParams(ActionContext.getParameters());
            if (!variables.isEmpty()) {
                settings.putAll(variables);
            }

            final ApplicationLink applicationLink = bambooApplicationLinkManager.getApplicationLink(projectObject.getKey());
            if (applicationLink != null) {
                final String errorMessage = triggerReleaseBuild(newBuild, applicationLink, settings);

                if (StringUtils.isNotBlank(errorMessage)) {
                    addErrorMessage(errorMessage);
                }
            }
        } else {
            bambooReleaseService.releaseWithNoBuild(version, settings);
        }

        if (hasAnyErrors()) {
            return doInput(); //prep data
        }

        return returnCompleteWithInlineRedirect(getRedirectUrlOnSuccess());
    }

    @VisibleForTesting
    String triggerReleaseBuild(boolean newBuild, final ApplicationLink applicationLink, final Map<String, String> settings) {
        try {
            PlanExecutionResult result;
            if (newBuild) {
                result = bambooReleaseService.triggerPlanForRelease(applicationLink, version, settings);
            } else {
                // Existing build
                final PlanResultKey planResultKey = PlanKeys.getPlanResultKey(buildResult);
                result = bambooReleaseService.triggerExistingBuildForRelease(
                        applicationLink, version, planResultKey, settings);
            }

            if (!result.getErrors().isEmpty()) {
                // Note: We are deliberately ignoring error details and showing a generic error to the user
                return getI18nText(BAMBOO_ERROR_CONNECTIVITY_CONTINUE_RELEASE_I18N_KEY, applicationLink.getName());
            }

            return null;
        } catch (CredentialsRequiredException e) {
            log.error(e.getMessage());
            return getI18nText(ERROR_USER_NOT_AUTHORISED_I18N_KEY, e.getAuthorisationURI().toString());
        } catch (Exception e) {
            log.error("Bamboo failed to trigger the release build for version " + (version != null ? version.getName() : ""), e);
            return getI18nText(JIRA_ERROR_NOT_RECOVERABLE_I18N_KEY, applicationLink.getName());
        }
    }

    protected void doValidation() {
        version = versionManager.getVersion(versionId);
        if (version == null) {
            addErrorMessage(getI18nText(ERROR_INVALID_VERSION_I18N_KEY, String.valueOf(versionId)));
        } else {
            if (BUILD_TYPE_NEW_BUILD.equals(buildType)) {
                final Project projectObject = version.getProject();
                if (projectObject == null) {
                    addErrorMessage(getI18nText(ERROR_VERSION_WITHOUT_PROJECT_I18N_KEY, version.getName()));
                }

                if (StringUtils.isBlank(selectedPlanKey)) {
                    addError("selectedPlanKey", getI18nText(ERROR_INVALID_PLAN_I18N_KEY));
                }
            }

            final Project project = version != null ? version.getProject() : null;
            if (project != null && !bambooReleaseService.hasPermissionToRelease(getLoggedInUser(), project)) {
                addErrorMessage(getI18nText(ADMIN_ERROR_VERSION_NO_PERMISSION_I18N_KEY));
            }

            validateReleaseDate();
        }

        if (hasAnyErrors()) {
            doInput();
        }
    }

    @VisibleForTesting
    String getI18nText(String key, String param1) {
        return getI18nHelper().getText(key, param1);
    }

    @VisibleForTesting
    String getI18nText(String key) {
        return getI18nHelper().getText(key);
    }

    private void validateReleaseDate() {
        if (userReleaseDate == null) {
            // The release date is not editable
            this.releaseDate = version.getReleaseDate();
        } else if (StringUtils.isBlank(userReleaseDate)) {
            // The user entered a blank release date
            this.releaseDate = null;
        } else {
            // The user entered a non-blank release date - parse and validate it
            try {
                final Date enteredReleaseDate = dateFieldFormat.parseDatePicker(userReleaseDate);
                // Parsing succeeded - validate against business rules
                validateReleaseDate(enteredReleaseDate);
            } catch (final IllegalArgumentException exc) {
                addError(RELEASE_DATE_FIELD,
                        getI18nText(INCORRECT_DATE_FORMAT, dateFieldFormat.getFormatHint()));
            }
        }
    }

    private void validateReleaseDate(final Date enteredReleaseDate) {
        final VersionBuilder versionBuilder = versionService.newVersionBuilder(version).releaseDate(enteredReleaseDate);
        final VersionBuilderValidationResult result = versionService.validateUpdate(getLoggedInUser(), versionBuilder);
        if (result.isValid()) {
            this.releaseDate = enteredReleaseDate;
        } else {
            addErrorCollection(result.getErrorCollection());
        }
    }

    /**
     * Returns the unresolved issues for the given version.
     *
     * @param projectId the project to which the version belongs
     * @param versionId the version for which to find the unresolved issues
     * @return zero if there is a problem getting this count
     */
    private int getUnresolvedIssues(final long projectId, final long versionId) {
        final Either<String, Integer> result = projectVersionService.getUnresolvedIssueCount(projectId, versionId);
        if (result.isRight()) {
            return result.right().get();
        }
        addErrorMessage(result.left().get());
        return 0;
    }

    private String getBaseUrl(final Project project, final Version version) {
        return String.format("/browse/%s/fixforversion/%s", project.getKey(), version.getId());
    }

    @VisibleForTesting
    static Map<String, String> filterVariableParams(final Map<?, ?> parameters) {
        return parameters.entrySet().stream()
                .filter(entry -> entry.getKey().toString().startsWith(VARIABLE_PARAM_PREFIX))
                .filter(entry -> entry.getValue() instanceof String[])
                .filter(entry -> {
                    final String[] values = (String[]) entry.getValue();
                    return values.length > 0 && isNotEmpty(values[0]);
                })
                .collect(
                        toMap(
                                entry -> entry.getKey().toString(),
                                entry -> ((String[]) entry.getValue())[0]
                        )
                );
    }

    @ActionViewData
    public Version getVersion() {
        return version;
    }

    @ActionRequestData
    public void setVersionId(long versionId) {
        this.versionId = versionId;
    }

    @ActionViewData
    public long getVersionId() {
        return versionId;
    }

    @ActionViewData
    public String getUnresolved() {
        return unresolved;
    }

    /**
     * Sets the action to take upon unresolved issues in this version, e.g. ignore or move.
     *
     * @param unresolved the action to take
     */
    @ActionRequestData
    public void setUnresolved(String unresolved) {
        this.unresolved = unresolved;
    }

    @ActionViewData
    public Long getMoveUnfixedIssuesTo() {
        return moveUnfixedIssuesTo;
    }

    /**
     * Sets the ID of the Version to which any unresolved issues should be moved.
     *
     * @param moveUnfixedIssuesTo null means do not move
     */
    @ActionRequestData
    public void setMoveUnfixedIssuesTo(@Nullable final Long moveUnfixedIssuesTo) {
        this.moveUnfixedIssuesTo = moveUnfixedIssuesTo;
    }

    @ActionViewData
    public String getRedirectUrlOnSuccess() {
        return redirectUrlOnSuccess;
    }

    @ActionRequestData
    public void setRedirectUrlOnSuccess(final String redirectUrlOnSuccess) {
        this.redirectUrlOnSuccess = redirectUrlOnSuccess;
    }

    @ActionViewData
    public String getBuildType() {
        return buildType;
    }

    @ActionRequestData
    public void setBuildType(String buildType) {
        this.buildType = buildType;
    }

    @ActionViewData
    public Map<BambooProject, List<BambooPlan>> getPlansByProject() {
        return plansByProject;
    }

    @ActionViewData
    public Collection<Version> getVersions() {
        return versions;
    }

    @ActionViewData
    public URI getCredentialsUrl() {
        return credentialsUrl;
    }

    @ActionViewData
    public int getOpenIssueCount() {
        return openIssueCount;
    }

    @ActionRequestData
    public void setSelectedPlanKey(String selectedPlanKey) {
        this.selectedPlanKey = selectedPlanKey;
    }

    @ActionViewData
    public String getSelectedPlanKey() {
        return selectedPlanKey;
    }

    @ActionViewData
    public String getVariablesJson() {
        return variablesJson;
    }

    @ActionViewData
    public String getSelectedStages() {
        return selectedStages;
    }

    @ActionRequestData
    public void setSelectedStages(final String selectedStages) {
        this.selectedStages = selectedStages;
    }

    @ActionRequestData("This radio button is created dynamically by JBAM.CONFIG.BuildResults in common.js")
    public void setBuildResult(final String buildResult) {
        this.buildResult = buildResult;
    }

    @ActionViewData
    public boolean isBambooLinked() {
        return bambooLinked;
    }

    // Doesn't seem to be used by releaseConfigDialog.vm
    @ActionViewData
    public String getUserReleaseDate() {
        return userReleaseDate;
    }

    @ActionRequestData
    public void setUserReleaseDate(final String userReleaseDate) {
        this.userReleaseDate = userReleaseDate;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    @ActionRequestData
    public void setDateFormat(final String dateFormat) {
        this.dateFormat = dateFormat;
    }

    @ActionViewData
    public String getCurrentReleaseDate() {
        return currentReleaseDate;
    }

    @ActionRequestData
    public void setCurrentReleaseDate(final String currentReleaseDate) {
        this.currentReleaseDate = currentReleaseDate;
    }

    @ActionViewData
    public String getFormattedReleaseDate() {
        return formattedReleaseDate;
    }

    @ActionViewData
    public String getApplicationLinkName() {
        return applicationLinkName;
    }
}
