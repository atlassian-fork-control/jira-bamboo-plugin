package com.atlassian.jira.plugin.ext.bamboo.deployments;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.web.action.issue.AbstractIssueSelectAction;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import static com.atlassian.jira.permission.ProjectPermissions.VIEW_DEV_TOOLS;
import static com.google.common.base.Preconditions.checkNotNull;

public class ViewIssueDeploymentStatus extends AbstractIssueSelectAction {
    private final IssueFactory issueFactory;

    private String applicationLinkId;
    private String deploymentProjectId;

    public ViewIssueDeploymentStatus(@ComponentImport final IssueFactory issueFactory) {
        this.issueFactory = checkNotNull(issueFactory);
    }

    @Override
    public String execute() throws Exception {
        final Issue issue = issueFactory.getIssue(getIssue());
        if (!getPermissionManager().hasPermission(VIEW_DEV_TOOLS, issue, getLoggedInUser())) {
            return PERMISSION_VIOLATION_RESULT;
        }
        return super.execute();
    }

    public boolean isJira5x() {
        return false;
    }

    public String getDeploymentProjectId() {
        return deploymentProjectId;
    }

    public void setDeploymentProjectId(final String deploymentProjectId) {
        this.deploymentProjectId = deploymentProjectId;
    }

    public String getApplicationLinkId() {
        return applicationLinkId;
    }

    public void setApplicationLinkId(final String applicationLinkId) {
        this.applicationLinkId = applicationLinkId;
    }
}
