package com.atlassian.jira.plugin.ext.bamboo.web;

import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.plugin.ext.bamboo.panel.BambooBuildResultsTabPanel;
import com.atlassian.jira.plugin.ext.bamboo.panel.BambooPanelHelper;
import com.atlassian.jira.plugin.ext.bamboo.service.BambooServerAccessor;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.software.api.conditions.ProjectDevToolsIntegrationFeatureCondition;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.velocity.htmlsafe.HtmlSafe;
import com.google.common.collect.Lists;
import com.opensymphony.module.sitemesh.RequestConstants;
import org.apache.log4j.Logger;
import webwork.action.ActionContext;

import java.net.URI;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.permission.ProjectPermissions.VIEW_DEV_TOOLS;
import static com.atlassian.jira.plugin.ext.bamboo.panel.BambooPanelHelper.SELECTED_SUB_TAB_KEY;
import static com.atlassian.jira.software.api.conditions.ProjectDevToolsIntegrationFeatureCondition.CONTEXT_KEY_PROJECT;
import static com.atlassian.sal.api.UrlMode.ABSOLUTE;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Collections.singletonMap;

/**
 * A single action to pull out HTML from a Bamboo builds response.
 */
@SuppressWarnings("unused")
public class ViewBambooPanelContent extends JiraWebActionSupport {
    private static final Logger log = Logger.getLogger(ViewBambooPanelContent.class);

    private final ApplicationProperties applicationProperties;
    private final BambooServerAccessor bambooServerAccessor;
    private final IssueManager issueManager;
    private final JiraAuthenticationContext authenticationContext;
    private final PermissionManager permissionManager;
    private final ProjectDevToolsIntegrationFeatureCondition projectDevToolsIntegrationFeatureCondition;
    private final ProjectManager projectManager;
    private final SearchService searchService;
    private final VersionManager versionManager;

    private boolean showRss;
    private Long versionId;
    private String bambooHtml;
    private String issueKey;
    private String projectKey;
    private String redirectUrl;
    private String selectedSubTab;

    public ViewBambooPanelContent(
            @ComponentImport final ApplicationProperties applicationProperties,
            @ComponentImport final IssueManager issueManager,
            @ComponentImport final JiraAuthenticationContext authenticationContext,
            @ComponentImport final PermissionManager permissionManager,
            @ComponentImport final ProjectManager projectManager,
            @ComponentImport final SearchService searchService,
            @ComponentImport final VersionManager versionManager,
            final BambooServerAccessor bambooServerAccessor,
            final ProjectDevToolsIntegrationFeatureCondition projectDevToolsIntegrationFeatureCondition) {
        this.applicationProperties = checkNotNull(applicationProperties);
        this.authenticationContext = checkNotNull(authenticationContext);
        this.bambooServerAccessor = checkNotNull(bambooServerAccessor);
        this.issueManager = checkNotNull(issueManager);
        this.permissionManager = checkNotNull(permissionManager);
        this.projectDevToolsIntegrationFeatureCondition = checkNotNull(projectDevToolsIntegrationFeatureCondition);
        this.projectManager = checkNotNull(projectManager);
        this.searchService = checkNotNull(searchService);
        this.versionManager = checkNotNull(versionManager);
    }

    @Override
    protected String doExecute() throws Exception {
        ActionContext.getRequest().setAttribute(RequestConstants.DECORATOR, "none");

        try {
            final String selectedSubTab = getSelectedSubTab();
            final String actionUrl;
            if (BambooPanelHelper.SUB_TAB_PLAN_STATUS.equals(selectedSubTab)) {
                actionUrl = BambooBuildResultsTabPanel.VIEW_PLANS_ACTION_URL;
            } else {
                actionUrl = BambooBuildResultsTabPanel.VIEW_BUILD_RESULTS_ACTION_URL;
            }

            final MapBuilder<String, String> extraParams = MapBuilder.newBuilder(
                    SELECTED_SUB_TAB_KEY, selectedSubTab,
                    "showRss", String.valueOf(showRss));

            if (issueKey != null) {
                final MutableIssue issueObject = issueManager.getIssueObject(issueKey);
                if (issueObject != null && hasViewBambooPanelPermission(issueObject.getProjectObject())) {
                    setBambooHtml(bambooServerAccessor.getHtmlFromAction(BambooBuildResultsTabPanel.VIEW_BUILD_RESULTS_ACTION_URL,
                            issueObject.getProjectObject(), getIssueKeys(issueObject), extraParams.toMap()));
                }

            } else if (versionId != null) {
                final Version version = versionManager.getVersion(versionId);
                if (version != null && hasViewBambooPanelPermission(version.getProject())) {
                    final Collection<String> issueKeys = getIssueKeys(version);
                    if (issueKeys != null && !issueKeys.isEmpty()) {
                        final Date releaseDate = version.getReleaseDate();
                        if (version.isReleased() && releaseDate != null) {
                            log.debug("Adding version release date " + releaseDate);

                            // Add the build date
                            Calendar c = Calendar.getInstance();
                            c.setTime(releaseDate);
                            c.add(Calendar.DAY_OF_MONTH, 1);
                            Date oneReleaseDate = c.getTime();

                            extraParams.add("releasedVersionTimestamp", String.valueOf(oneReleaseDate.getTime()));
                        }

                        setBambooHtml(bambooServerAccessor.getHtmlFromAction(actionUrl, version.getProject(), issueKeys, extraParams.toMap()));
                    } else {
                        setBambooHtml(getText("bamboo.panel.buildByDate.noJiraIssues"));
                    }
                }
            } else if (projectKey != null) {
                Project project = projectManager.getProjectObjByKey(projectKey);
                if (project != null && hasViewBambooPanelPermission(project)) {
                    bambooHtml = bambooServerAccessor.getHtmlFromAction(actionUrl, project, extraParams.toMap());
                }
            }
        } catch (CredentialsRequiredException e) {
            log.info("Credentials are required but cannot be found. Redirecting to authorization page.");

            final String baseLinkUrl = ActionContext.getRequest().getParameter("baseLinkUrl");
            final String jiraBaseUrl = applicationProperties.getBaseUrl(ABSOLUTE);
            final String callbackUrl = jiraBaseUrl + baseLinkUrl;
            final String redirect = e.getAuthorisationURI(URI.create(callbackUrl)).toASCIIString();
            setRedirectUrl(redirect);

            return PERMISSION_VIOLATION_RESULT;
        } catch (Exception e) {
            //let's never leave the Builds tab spinning infinitely. Tell the user that an error occurred whenever it happens.
            log.warn("Unable to to connect to Bamboo server. Nothing will be shown.", e);
            addErrorMessage(getText("bamboo.panel.connection.error"));
            return ERROR;
        }

        return super.doExecute();
    }

    private boolean hasViewBambooPanelPermission(final Project project) {
        final Map<String, Object> projectContext = singletonMap(CONTEXT_KEY_PROJECT, project);
        return permissionManager.hasPermission(VIEW_DEV_TOOLS, project, authenticationContext.getLoggedInUser())
                && projectDevToolsIntegrationFeatureCondition.shouldDisplay(projectContext);
    }

    private Collection<String> getIssueKeys(Version version) {
        try {
            final JqlQueryBuilder builder = JqlQueryBuilder.newBuilder().where().
                    project(version.getProject().getId()).and().fixVersion(version.getId()).endWhere();

            final SearchResults<Issue> results = searchService.search(
                    authenticationContext.getLoggedInUser(), builder.buildQuery(), PagerFilter.getUnlimitedFilter());
            final Collection<Issue> issues = results.getResults();
            final List<String> issueKeys = Lists.newArrayListWithExpectedSize(issues.size());
            for (final Issue issue : issues) {
                issueKeys.addAll(getIssueKeys(issue));
            }
            return issueKeys;
        } catch (SearchException e) {
            log.warn("Unable to get all issues from version " + version + ". Returning null.", e);
            return null;
        }
    }

    private Collection<String> getIssueKeys(final Issue issue) {
        return issueManager.getAllIssueKeys(issue.getId());
    }

    public String getSelectedSubTab() {
        return selectedSubTab;
    }

    public void setSelectedSubTab(String selectedSubTab) {
        this.selectedSubTab = selectedSubTab;
    }

    @HtmlSafe
    public String getBambooHtml() {
        return bambooHtml;
    }

    private void setBambooHtml(String bambooHtml) {
        this.bambooHtml = bambooHtml == null ? "" : bambooHtml;
    }

    public String getIssueKey() {
        return issueKey;
    }

    public void setIssueKey(String issueKey) {
        this.issueKey = issueKey;
    }

    public String getProjectKey() {
        return projectKey;
    }

    public void setProjectKey(String projectKey) {
        this.projectKey = projectKey;
    }

    public Long getVersionId() {
        return versionId;
    }

    public void setVersionId(Long versionId) {
        this.versionId = versionId;
    }

    public boolean isShowRss() {
        return showRss;
    }

    public void setShowRss(boolean showRss) {
        this.showRss = showRss;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }
}
