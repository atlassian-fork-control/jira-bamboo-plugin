package com.atlassian.jira.plugin.ext.bamboo.upgrade.legacy.util;

import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.module.propertyset.PropertySetManager;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Implementation of the LegacyBambooPropertyManager. This should only be used by the upgrade tasks.
 */
@Component
@SuppressWarnings("unused")
public class LegacyBambooPropertyManagerImpl implements LegacyBambooPropertyManager {
    /**
     * Locate PropertySet using PropertyStore for this sequenceName/sequenceId mapping.
     */
    private static PropertySet loadPropertySet() {
        final Map<String, Object> psArgs = new HashMap<String, Object>();
        psArgs.put("delegator.name", "default");
        psArgs.put("entityName", "BambooServerProperties");
        psArgs.put("entityId", 1L);

        return PropertySetManager.getInstance("ofbiz", psArgs);
    }

    private final PropertySet propertySet;

    public LegacyBambooPropertyManagerImpl() {
        propertySet = loadPropertySet();
    }

    public PropertySet getPropertySet() {
        return propertySet;
    }
}
